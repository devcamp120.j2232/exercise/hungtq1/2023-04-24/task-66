package com.devcamp_task66.province_district_ward.service;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp_task66.province_district_ward.model.District;
import com.devcamp_task66.province_district_ward.model.Ward;
import com.devcamp_task66.province_district_ward.repository.DistrictRepository;
import com.devcamp_task66.province_district_ward.repository.WardRepositoty;

@Service
public class WardService  {
    
    @Autowired
    WardRepositoty wardRepositoty;

    @Autowired
    DistrictRepository districtRepository;

    public ArrayList<Ward> getWardList(){
        ArrayList<Ward> listWard = new ArrayList<>();
        wardRepositoty.findAll().forEach(listWard::add);
        return listWard;
    }

    public Set<Ward> getWardByDistrictId(int id){
        District district = districtRepository.findById(id);
        if(district != null){
            return district.getWards();
        }
        return null;
    }
}
