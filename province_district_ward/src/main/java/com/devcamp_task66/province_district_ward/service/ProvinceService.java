package com.devcamp_task66.province_district_ward.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp_task66.province_district_ward.model.Province;
import com.devcamp_task66.province_district_ward.repository.ProvinceRepository;

@Service
public class ProvinceService {
    
    @Autowired
    ProvinceRepository provinceRepository;

    public ArrayList<Province> getProvinceList(){
        ArrayList<Province> listProvince = new ArrayList<>();
        provinceRepository.findAll().forEach(listProvince::add);
        return listProvince;
    }

    
}
