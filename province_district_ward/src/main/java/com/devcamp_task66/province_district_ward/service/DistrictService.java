package com.devcamp_task66.province_district_ward.service;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp_task66.province_district_ward.model.District;
import com.devcamp_task66.province_district_ward.model.Province;
import com.devcamp_task66.province_district_ward.repository.DistrictRepository;
import com.devcamp_task66.province_district_ward.repository.ProvinceRepository;

@Service
public class DistrictService {
    
    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    ProvinceRepository provinceRepository;

    public ArrayList<District> getDistrictList(){
        ArrayList<District> listDistrict = new ArrayList<>();
        districtRepository.findAll().forEach(listDistrict::add);
        return listDistrict;
    }

    public Set<District> getDistrictByProId(int id){
        Province province = provinceRepository.findById(id);
        if (province != null){
            return province.getDistricts();
        
        }else{
            return null;
        }
    }
      
}
