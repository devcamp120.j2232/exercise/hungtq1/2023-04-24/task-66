package com.devcamp_task66.province_district_ward.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp_task66.province_district_ward.model.Province;

public interface ProvinceRepository extends JpaRepository<Province, Integer>{
            Province findById(int id);
}
