package com.devcamp_task66.province_district_ward.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp_task66.province_district_ward.model.District;

public interface DistrictRepository extends JpaRepository<District, Integer>{
    District findById(int id);
}
