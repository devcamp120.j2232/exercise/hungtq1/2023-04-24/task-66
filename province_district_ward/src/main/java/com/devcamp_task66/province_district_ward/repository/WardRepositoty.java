package com.devcamp_task66.province_district_ward.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp_task66.province_district_ward.model.Ward;

public interface WardRepositoty extends JpaRepository<Ward, Integer> {
    Ward findById(int id);
}
