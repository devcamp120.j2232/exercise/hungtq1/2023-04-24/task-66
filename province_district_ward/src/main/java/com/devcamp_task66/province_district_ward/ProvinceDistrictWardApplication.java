package com.devcamp_task66.province_district_ward;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProvinceDistrictWardApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProvinceDistrictWardApplication.class, args);
	}

}
