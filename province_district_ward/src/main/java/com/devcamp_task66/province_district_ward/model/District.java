package com.devcamp_task66.province_district_ward.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "district")
public class District {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "prefix")
    private String prefix;

    @ManyToOne
    private Province province;
   
    @OneToMany(targetEntity = Ward.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "district_id")
    @JsonIgnore
    private Set<Ward> wards;

    public District() {
    }


    public District(int id, String name, String prefix, Province province, Set<Ward> wards) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
        this.province = province;
        this.wards = wards;
    }


    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getPrefix() {
        return prefix;
    }


    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }


    public Province getProvince() {
        return province;
    }


    public void setProvince(Province province) {
        this.province = province;
    }


    public Set<Ward> getWards() {
        return wards;
    }


    public void setWards(Set<Ward> wards) {
        this.wards = wards;
    }

}
