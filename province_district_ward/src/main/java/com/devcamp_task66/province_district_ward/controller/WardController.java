package com.devcamp_task66.province_district_ward.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp_task66.province_district_ward.repository.DistrictRepository;
import com.devcamp_task66.province_district_ward.repository.ProvinceRepository;
import com.devcamp_task66.province_district_ward.repository.WardRepositoty;
import com.devcamp_task66.province_district_ward.service.WardService;
import com.devcamp_task66.province_district_ward.model.District;
import com.devcamp_task66.province_district_ward.model.Ward;

@RequestMapping("/")
@RestController
@CrossOrigin
public class WardController {
    
    @Autowired
    WardRepositoty wardRepositoty;

    @Autowired
    WardService wardService;

    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    ProvinceRepository provinceRepository;

    @GetMapping("/all-ward")
    public ResponseEntity <List<Ward>> getAllWards(){
        try {
            return new ResponseEntity<>(wardService.getWardList(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/ward/{id}")
    public ResponseEntity<Object> getWardById(@PathVariable(name = "id") int id){
        Ward ward = wardRepositoty.findById(id);
        if(ward != null){
            return new ResponseEntity<>(ward, HttpStatus.OK);
        }else{
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/ward-by-dist-id/{id}")
    public ResponseEntity<Set<Ward>> getWardByDistrictId(@PathVariable("id") int id){
        District district = districtRepository.findById(id);
        try {
            if(district != null){
                return new ResponseEntity<>(wardService.getWardByDistrictId(id), HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping("/ward-create/{districtId}")
    public ResponseEntity<Object> createWard(@PathVariable(value = "districtId") int districtId,  @RequestBody Ward pWard){
        try {
            District district = districtRepository.findById(districtId);
            if(district != null){
                Ward newWard = new Ward();
                newWard.setName(pWard.getName());
                newWard.setPrefix(pWard.getPrefix());
                newWard.setDistrict(district);
                return new ResponseEntity<>(wardRepositoty.save(newWard), HttpStatus.CREATED);
            }
            else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/ward-update/{wardId}/{districtId}")
    public ResponseEntity<Object> wardUpdate(@PathVariable("wardId") int wardId, @PathVariable("districtId") int districtId, @RequestBody Ward pWard ){
        try {
            Ward ward = wardRepositoty.findById(wardId);
            District district = districtRepository.findById(districtId);
            if(ward != null){
                ward.setName(pWard.getName());
                ward.setPrefix(pWard.getPrefix());
                ward.setDistrict(district);
                return new ResponseEntity<>(wardRepositoty.save(ward), HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/ward-delete/{id}")
    public ResponseEntity<Ward> deleteProvince(@PathVariable("id") int id){
        try {
            Ward ward = wardRepositoty.findById(id);
            wardRepositoty.delete(ward);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);          
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
