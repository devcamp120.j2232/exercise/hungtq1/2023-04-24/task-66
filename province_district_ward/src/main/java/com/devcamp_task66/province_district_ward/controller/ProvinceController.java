package com.devcamp_task66.province_district_ward.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp_task66.province_district_ward.model.Province;
import com.devcamp_task66.province_district_ward.repository.DistrictRepository;
import com.devcamp_task66.province_district_ward.repository.ProvinceRepository;
import com.devcamp_task66.province_district_ward.service.ProvinceService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ProvinceController {
    
    @Autowired
    ProvinceRepository provinceRepository;

    @Autowired
    ProvinceService provinceService;

    @Autowired
    DistrictRepository districtRepository;

    @GetMapping("/all-provinces")
    public ResponseEntity<List<Province>> getAllProvinces(){
        try {
            return new ResponseEntity<>(provinceService.getProvinceList(), HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/province/{id}")
    public ResponseEntity<Object> getProvinceById(@PathVariable(name = "id") int id){
        Province province = provinceRepository.findById(id);
        if (province != null){
            return new ResponseEntity<>(province, HttpStatus.OK);
        }else{
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }


    @PostMapping("/province-create")
    public ResponseEntity<Province> createProvince(@RequestBody Province pProvince){
        try {
            Province newProvince = new Province();
            newProvince.setCode(pProvince.getCode());
            newProvince.setName(pProvince.getName());
            //newProvince.setId(pProvince.getId());
            return new ResponseEntity<>(provinceRepository.save(newProvince), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/province-update/{id}")
    public ResponseEntity<Object> updateProvince(@PathVariable("id") int id, @RequestBody Province pProvince){
        try {
            Province province = provinceRepository.findById(id);
            if (province != null){
                province.setName(pProvince.getName());
                province.setCode(pProvince.getCode());
                return new ResponseEntity<>(provinceRepository.save(province), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/province-delete/{id}")
    public ResponseEntity<Province> deleteProvince(@PathVariable("id") int id){
        try {
            Province province = provinceRepository.findById(id);
            provinceRepository.delete(province);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}