package com.devcamp_task66.province_district_ward.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import com.devcamp_task66.province_district_ward.model.District;
import com.devcamp_task66.province_district_ward.model.Province;
import com.devcamp_task66.province_district_ward.repository.DistrictRepository;
import com.devcamp_task66.province_district_ward.repository.ProvinceRepository;
import com.devcamp_task66.province_district_ward.service.DistrictService;

@RequestMapping("/")
@RestController
@CrossOrigin
public class DistrictController {
    
    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    DistrictService districtService;

    @Autowired
    ProvinceRepository provinceRepository;

    @GetMapping("/all-districts")
    public ResponseEntity <List<District>> getAllDistricts(){
        try {
            return new ResponseEntity<>(districtService.getDistrictList(), HttpStatus.OK);
        } catch (Exception e) {
           return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);     
        }
    }

    @GetMapping("/district/{id}")
    public ResponseEntity<Object> getDistrictById(@PathVariable(name = "id") int id){
        District district = districtRepository.findById(id);
        if (district != null){
            return new ResponseEntity<>(district, HttpStatus.OK);
        }else{
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/dist-by-prov-id/{id}")
    public ResponseEntity<Set<District>> getDistrictByProvinceId(@PathVariable("id") int id){
        Province province = provinceRepository.findById(id);
        try {
            if(province != null){
                return new ResponseEntity<>(districtService.getDistrictByProId(id), HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping("/district-create/{id}")
    public ResponseEntity<Object> createDistrict(@PathVariable(name = "id") int id, @RequestBody District pDistrict){
        Province province = provinceRepository.findById(id);
        try {
            if(province != null){
            District newDistrict = new District();
            newDistrict.setName(pDistrict.getName());
            newDistrict.setPrefix(pDistrict.getPrefix());
            newDistrict.setProvince(province);
            return new ResponseEntity<>(districtRepository.save(newDistrict), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }                
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/district-update/{districtId}/{provinceId}")
    public ResponseEntity<Object> updateDistrict(@PathVariable(value = "districtId") int districtId, @PathVariable(value = "provinceId") int provinceId,  @RequestBody District pDistrict){
        try {
            District district = districtRepository.findById(districtId);
            Province province = provinceRepository.findById(provinceId);
            if(district != null){
                district.setName(pDistrict.getName());
                district.setPrefix(pDistrict.getPrefix());
                district.setProvince(province);
                return new ResponseEntity<>(districtRepository.save(district), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 
    
    @DeleteMapping("/district-delete/{id}")
    public ResponseEntity<District> deleteDistrict(@PathVariable("id") int id){
        try {
            District district = districtRepository.findById(id);
            districtRepository.delete(district);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);          
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
